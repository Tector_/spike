# Quick Start

**Python 3.5.3 or higher is required**

You can clone Spike repo with your git client.

```ml
git clone https://bitbucket.org/dismadbot/spike.git
```
or
```ml
git clone git@bitbucket.org:dismadbot/spike.git
```

Install requirement

```ml
pip install -r requirements.txt
```
Start bot
```ml
python3 < botName.py >
```

# Bot list
### Main Bot
These bot can interact directly with users

* User Bot reply to s! commands ```runBot.py```
* Admin Bot reply to @! commands ```runAdminBot.py```
* Super Admin Bot reply to su! commands ```runSuperAdminBot.py```
* Spike Blood bowl League reply to m! commands in SBBL server only ```runSBBLBot.py```
* Twitch bot reply to ! commands on poncho_dlv_ twitch channel ```runTwitchBot.py```

### Cron Bot
These bot are start by crontab for regular task like update or notification.

* Auto register competitions in spike if the option is enabled ```runAutoreg.py```
* Manage Bounty update after a match ```runBountyCheck.py```
* Update discord guild info for spike website ```runDiscordGuildUpdate.py```
* Auto register competition in goblin spy ```runGoblinSpyRegistration.py.py```
* Generate match report for all league ```runMatchReport.py```
* Display youtube/twitch notification ```runNotifyBot.py```
* Update match history from goblinspy for cabalvision league ```runUpdateCabalvisionFromGS.py```
* Update scheduled matches for all known leagues ```runUpdateContest.py```
* Update match history from match report ```runUpdateMhFromMr.py```
* Update match history from cyanide api ```runUpdateMhFromWs.py```

### Manual Bot
These bot are started manually for punctual actions

* Update resources database ```runCommonDbFiller.py```
* Update match history from goblinspy databases ```runUpdateMhFromGS.py```
* database migration from sqlite to MongoDb ```runMongoMigration.py```
