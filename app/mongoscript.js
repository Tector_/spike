#replace platform label by id
db.getCollection('match_history').find({platform: "pc"}).forEach( function (x) {x.platform = 1;db.getCollection('match_history').save(x);});
db.getCollection('match_history').find({platform: "ps4"}).forEach( function (x) {x.platform = 2;db.getCollection('match_history').save(x);});
db.getCollection('match_history').find({platform: "xb1"}).forEach( function (x) {x.platform = 3;db.getCollection('match_history').save(x);});

#cast platform from string to int
db.getCollection('match_history').find({"platform":{"$type": 1}}).forEach( function(x) {x.platform = new NumberInt(x.platform);db.getCollection('match_history').save(x);});

#cast string id in int
db.getCollection('coaches').find({"id":{"$type": 2}}).forEach( function(x) {x.id = new NumberInt(x.id);db.getCollection('coaches').save(x);});

#cast durationid in int
db.getCollection('match_history').find({}).forEach( function(x) {print("id: " + x._id);x.duration = new NumberInt(x.duration);db.getCollection('match_history').save(x);});

#Remove duplicate coaches
db.coaches.find({}, {'id' : 1, 'platform_id': 1}).sort({_id:1}).forEach(function(doc){print("id: " + doc.id + " platform_id: " + doc.platform_id);db.coaches.remove({_id:{$gt:doc._id}, id:doc.id, platform_id:doc.platform_id});})

#index coaches
db.coaches.ensureIndex({"id": 1, "platform_id": 1}, {unique: true, dropDups: true})

#index contests
db.contests.ensureIndex({"contest_id": 1, "platform_id": 1}, {unique: true, dropDups: true})

#index bounty
db.bounty.ensureIndex({"player_id": 1, "platform_id": 1}, {unique: true, dropDups: true})

#index team
db.teams.ensureIndex({"id": 1, "platform_id": 1}, {unique: true, dropDups: true})

#index player
db.players.ensureIndex({"id": 1, "platform_id": 1}, {unique: true, dropDups: true})

#index matchup
db.matchup.ensureIndex({"home_team": 1, "away_team": 1}, {unique: true, dropDups: true})

db.users.ensureIndex({"discord_id": 1}, {unique: true, dropDups: true})

#match history
db.match_history.ensureIndex({"platform": 1, "league_name": 1, "duration": 1}, {unique: false, dropDups: false})
db.match_history.ensureIndex({"platform": 1, "league_name": 1}, {unique: false, dropDups: false})
db.match_history.ensureIndex({"platform": 1, "team_away.coach_id": 1, "team_home.coach_id": 1, "duration": 1}, {unique: false, dropDups: false})

#competiton
db.competitions.ensureIndex({"league_id": 1, "platform_id": 1, "name": 1}, {unique: false, dropDups: false})

#Matches
db.matches.ensureIndex({"match_data.match.idcompetition": 1, "match_data.match.platform": 1}, {unique: false, dropDups: false})
db.matches.ensureIndex({"match_data.match.idleague": 1, "match_data.match.platform": 1}, {unique: false, dropDups: false})

#remove competiton field
db.leagues.update({}, {"$unset": {"competition":1}}, {"multi": true});

#remove league without id
db.leagues.remove({id: {"$exists": false}})

#Hide old contest
db.contests.find({"match_uuid": {"$exists": false}}, {"league_id": 1, 'competition_id' : 1, 'platform_id': 1, "current_round": 1}).sort({_id:1}).forEach(function(doc){print("league_id: " + doc.league_id + " competition_id: " + doc.competition_id + " platform_id: " + doc.platform_id + " current_round: " + doc.current_round);db.contest.update({league_id:doc.league_id, platform_id:doc.platform_id, competition_id: doc.competition_id, current_round:{$lt:doc.current_round}}, {match_uuid: null});})

#get league with int value as name
db.getCollection('leagues').find({"name":{"$type": 16}})

#Remove duplicate leagues
db.leagues.find({}, {'name' : 1, 'platform_id': 1}).sort({_id:1}).forEach(function(doc){print("name: " + doc.name + " platform_id: " + doc.platform_id);db.leagues.remove({_id:{$gt:doc._id}, name:doc.name, platform_id:doc.platform_id});})

#Reset update indicator
db.getCollection('matches').find({}).sort({_id:-1}).limit(90000).forEach( function (x) {x.team_player_updated = new NumberInt(0);db.getCollection('matches').save(x);});

#Cast objectId
db.getCollection('players').find({}).forEach(function(x) {
print("id: " + x._id);
x.starting_skills = x.starting_skills.map((skillId) => {
  return new ObjectId(skillId);
});
db.getCollection('players').save(x);
});


db.getCollection('star_players').find({}).forEach(function(x) {
print("id: " + x._id);
x.starting_skills = x.starting_skills.map((skillId) => {
  return new ObjectId(skillId);
});
db.getCollection('star_players').save(x);
});



db.getCollection('races').find({}).forEach(function(x) {
print("id: " + x._id);
x.star_players = x.star_players.map((sp_id) => {
  return new ObjectId(sp_id);
});
x.roster = x.roster.map((roster_id) => {
  return new ObjectId(roster_id);
});
x.inducements = x.inducements.map((induce_id) => {
  return new ObjectId(induce_id);
});
db.getCollection('races').save(x);
});