#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from discord.ext.commands import command, guild_only, Bot, Cog, check

import spike_exception
from __init__ import spike_logger, spike_command_logger, init_log_file_handler, except_hook, intents
from spike_command.Common import Common as Cm_cmd
from spike_command.Competition import Competition as Cmp_cmd
from spike_command.League import League as Lg_cmd
from spike_command.Schedule import Schedule as Sched_cmd
from spike_command.Video import Video as Vid_cmd
from spike_event.EventCommon import EventCommon
from spike_settings.DiscordGuildSettings import DiscordGuildSettings
from spike_settings.SpikeSettings import SpikeSettings
from discord_resources.Utilities import DiscordUtilities

DEBUG_MODE = False

# Discord client to communicate with server
client = Bot(command_prefix="@!", intents=intents)

locked_user = []
Already_running = False


async def log_command(ctx):
    abuse_list = SpikeSettings.get_abuse_list()
    if ctx.author.id in abuse_list:
        return False
    if ctx.guild is not None:
        spike_command_logger.info("{} :: {}".format(ctx.command, ctx.guild.name))
    else:
        spike_command_logger.info("{} :: Private message".format(ctx.command))
    return True


def check_admin_right(ctx):
    utils = DiscordUtilities(client)
    if utils.is_bot_admin(ctx):
        return True
    else:
        settings = DiscordGuildSettings(ctx.guild.id)
        raise spike_exception.InvalidRightError(settings.get_language())


class Competition(Cog):
    """Competition documentations"""

    @command()
    @check(log_command)
    @check(check_admin_right)
    @guild_only()
    async def autoreg(self, ctx):
        """Register all competition of all registered league

            Spike will ask you a channel to report match for each registered leagues
        """
        cmd = Cmp_cmd(client)
        await cmd.auto_register_competition(ctx)

    @command()
    @check(log_command)
    @check(check_admin_right)
    @guild_only()
    async def add_cmp(self, ctx, *, args):
        """Register new competition in Spike

            Mandatory argument:
                Competition name:   Approximate name of the competition to register
            Platform: (By default use league platform)
                    pc      Competition is in PC game
                    ps4     Competition is in Ps4 game
                    xb1     Competition is in xBox game

            Example:
                @!add_cmp champ ladder
        """
        cmd = Cmp_cmd(client)
        await cmd.add_competition(ctx, args)

    @command()
    @check(log_command)
    @check(check_admin_right)
    @guild_only()
    async def rm_cmp(self, ctx, *, args):
        """Remove a registered competition

            Mandatory argument:
                Competition name: approximate name of registered competition

            Optional argument:
                Platform: (By default use league platform)
                    pc      Team is in PC game
                    ps4     Team is in Ps4 game
                    xb1     Team is in xBox game
            Example:
                @!rm_cmp champ lad 17
                @!rm_cmp champ lad 17 xb1
        """
        cmd = Cmp_cmd(client)
        await cmd.remove_competition(ctx, args)

    @command()
    @check(log_command)
    @check(check_admin_right)
    @guild_only()
    async def rm_all_cmp(self, ctx):
        """Remove all registered competition
            Example:
                @!rm_all_cmp
        """
        cmd = Cmp_cmd(client)
        await cmd.remove_all_competitions(ctx)

    @command()
    @check(log_command)
    @check(check_admin_right)
    @guild_only()
    async def cmps(self, ctx):
        """Display the list of registered competitions

            "Registered competitions" are the competitions already registered in spike
            "Active competition" are the competitions created or running in Blood Bowl 2

        """
        cmd = Cmp_cmd(client)
        await cmd.competition_list(ctx)

    @command()
    @check(log_command)
    @check(check_admin_right)
    @guild_only()
    async def teams(self, ctx, *, competition, platform=None):
        """Display the list of team in a competition

            Mandatory argument:
                competition_name:   Approximate name of the competition
            Example:
                @!teams champ cup
        """
        cmd = Cm_cmd(client)
        await cmd.team_list(ctx, competition)

    @command()
    @check(log_command)
    @check(check_admin_right)
    @guild_only()
    async def coaches(self, ctx, *, competition, platform=None):
        """Display the list of coach in a competition

            Mandatory argument:
                competition_name:   Approximate name of the competition
            Example:
                @!coaches champ cup

        """
        cmd = Cm_cmd(client)
        await cmd.coaches(ctx, competition)

    @command()
    @check(log_command)
    @check(check_admin_right)
    @guild_only()
    async def cal(self, ctx, *, args):
        """Display match calendar for the given competition

            Mandatory argument:
                Competition name: approximate name of registered competition

            Optional argument:
                Platform: (By default use league platform)
                    pc      Team is in PC game
                    ps4     Team is in Ps4 game
                    xb1     Team is in xBox game

                Example:
                    @!cal spike bowl
                    @!cal champ cup ps4
        """
        cmd = Sched_cmd(client)
        await cmd.schedule_competition(ctx, args, True)


class League(Cog):
    """League documentations"""

    @command()
    @check(log_command)
    @check(check_admin_right)
    @guild_only()
    async def add_lg(self, ctx, *, args):
        """Register new league in Spike

            if you want report to multiple league it’s important to add
            the different league. Competition can only be added from
            league previously added on Spike!

            Mandatory argument:
                League name:   Approximate name of the league to register
            Optional argument:
                Platform: (By default use league platform)
                    pc      League is on PC
                    ps4     League is on Ps4
                    xb1     League is on xBox

            Example:
                @!add_lg cabalvision offi
        """
        cmd = Lg_cmd(client)
        await cmd.add_league(ctx, args)

    @command()
    @check(log_command)
    @check(check_admin_right)
    @guild_only()
    async def rm_lg(self, ctx, *, args):
        """Remove a registered league and containing competitions

            Mandatory argument:
                League name: approximate name of registered league

            Optional argument:
                Platform: (By default use league platform)
                    pc      Team is in PC game
                    ps4     Team is in Ps4 game
                    xb1     Team is in xBox game
            Example:
                @!rm_lg cabalvision offi
                @!rm_lg cabalvision offi xb1
        """
        cmd = Lg_cmd(client)
        await cmd.remove_league(ctx, args)

    @command()
    @check(log_command)
    @check(check_admin_right)
    @guild_only()
    async def rm_all_lg(self, ctx):
        """Remove all registered leagues and all registered competitions
            Example:
                @!rm_all_lg
        """
        cmd = Lg_cmd(client)
        await cmd.remove_all_leagues(ctx)
        cmd = Cmp_cmd(client)
        await cmd.remove_all_competitions(ctx)

    @command()
    @check(log_command)
    @check(check_admin_right)
    @guild_only()
    async def lgs(self, ctx):
        """Display the list of registered leagues"""
        cmd = Lg_cmd(client)
        await cmd.leagues(ctx)


class Administration(Cog):
    """Administration documentation"""

    @command()
    @check(log_command)
    @check(check_admin_right)
    @guild_only()
    async def prune(self, ctx, nb_messages: int, user=None):
        """Remove messages

         Mandatory argument:
                nb_messages   Number of message to delete

            Optional argument:
                user     remove messages from specific user, you have to mention the user

            Example:
                @!prune 10
                @!prune 10 @Spike

        """
        cmd = Cm_cmd(client)
        print("user {} try to prune on {}".format(ctx.author.name, ctx.guild.id))
        await cmd.prune(ctx, nb_messages, user)


class Streaming(Cog):
    """Streaming documentation"""

    @command()
    @check(log_command)
    @check(check_admin_right)
    @guild_only()
    async def add_yt(self, ctx, *, channel_id):
        """Register new youtube channel

            Mandatory argument:
                channel_id   Youtube id of the channel

            Example:
                @!add_yt UC6Y6d8Wod3jYHAUtSac6IfA
        """
        cmd = Vid_cmd(client)
        await cmd.add_youtube_channel(ctx, channel_id)

    @command()
    @check(log_command)
    @check(check_admin_right)
    @guild_only()
    async def rm_yt(self, ctx, *, channel_id):
        """Remove registered youtube channel

            Mandatory argument:
                channel_id   Youtube id of the channel

            Example:
                @!rm_yt UC6Y6d8Wod3jYHAUtSac6IfA
        """
        cmd = Vid_cmd(client)
        await cmd.remove_youtube_channel(ctx, channel_id)

    @command()
    @check(log_command)
    @check(check_admin_right)
    @guild_only()
    async def add_twitch(self, ctx, *, channel_name):
        """Register new twitch channel

            Mandatory argument:
                channel_name   Twitch channel name

            Example:
                @!add_twitch poncho_dlv_
        """
        cmd = Vid_cmd(client)
        await cmd.add_twitch_channel(ctx, channel_name)

    @command()
    @check(log_command)
    @check(check_admin_right)
    @guild_only()
    async def rm_twitch(self, ctx, *, channel_name):
        """Remove registered twitch channel

            Mandatory argument:
                channel_name   Twitch channel name

            Example:
                @!rm_twitch poncho_dlv_
        """
        cmd = Vid_cmd(client)
        await cmd.unregister_twitch_channel(ctx, channel_name)

    @command()
    @check(log_command)
    @check(check_admin_right)
    @guild_only()
    @check(log_command)
    async def videos(self, ctx):
        """List of registered channel"""
        cmd = Vid_cmd(client)
        await cmd.videos(ctx)


client.add_cog(Administration())
client.add_cog(Competition())
client.add_cog(League())
client.add_cog(Streaming())


def main():
    init_log_file_handler("admin_bot")
    client.run(SpikeSettings.get_discord_token())


@client.event
async def on_command_error(ctx, error):
    spike_logger.error("on_command_error: {}".format(error))
    event = EventCommon(client)
    await event.on_command_error(ctx, error, "@!")


@client.event
async def on_member_join(member):
    event = EventCommon(client)
    await event.on_member_join(member)


@client.event
async def on_member_remove(member):
    event = EventCommon(client)
    await event.on_member_remove(member)


@client.event
async def on_raw_reaction_add(raw_reaction):
    event = EventCommon(client)
    global locked_user
    locked_user = await event.on_raw_reaction_add(raw_reaction, locked_user)


@client.event
async def on_ready():
    global Already_running
    if not DEBUG_MODE and not Already_running:
        spike_logger.info("admin_bot connected")
        Already_running = True
        event = EventCommon(client)
        await event.on_ready()
    else:
        spike_logger.warning("admin_bot reconnect")


@client.event
async def on_guild_join(server):
    spike_logger.info("on_guild_join: {} - {}".format(server.name, server.id))
    event = EventCommon(client)
    await event.on_guild_join(server)


@client.event
async def on_guild_remove(server):
    spike_logger.info("on_guild_remove: {} - {}".format(server.name, server.id))
    event = EventCommon(client)
    await event.on_guild_remove(server)


sys.excepthook = except_hook

if __name__ == "__main__":
    main()
