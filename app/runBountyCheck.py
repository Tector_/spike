#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

import discord

import discord_resources
from __init__ import spike_logger, init_log_file_handler, except_hook, intents
from discord_resources.Utilities import DiscordUtilities
from spike_database.Bounty import Bounty, BountyStatus
from spike_database.Matches import Matches
from spike_database.ResourcesRequest import ResourcesRequest
from spike_model.Match import Match
from spike_requester.Utilities import Utilities
from spike_settings.SpikeSettings import SpikeSettings

client = discord.Client(intents=intents)

# Avoid restart the update in case of web socket restart
Already_running = False


def main():
    init_log_file_handler("bounty_check")
    client.run(SpikeSettings.get_discord_token())


@client.event
async def on_ready():
    global Already_running
    if not Already_running:
        spike_logger.info("bounty_check connected")
        Already_running = True
        await check_bounty()
    else:
        spike_logger.warning("bounty_check reconnect")


async def check_bounty():
    await client.wait_until_ready()
    matches_db = Matches()
    matches = matches_db.get_match_to_check_for_bounty()
    match_ids = []

    try:
        for match in matches:
            uuid = match["uuid"]
            match = Match(match)
            match.set_uuid(uuid)
            match_ids.append(uuid)

            if match is not None:
                spike_logger.info("check match {}".format(match.get_uuid()))
                if match.get_team_home() is not None:
                    await check_bounty_in_team(match, match.get_team_home())
                if match.get_team_away() is not None:
                    await check_bounty_in_team(match, match.get_team_away())
            else:
                spike_logger.error("Error with match {}".format(uuid))
    except Exception as e:
        spike_logger.error("Error with match {}".format(e))
        matches_db.set_match_checked_for_bounty(match_ids)

    matches_db.set_match_checked_for_bounty(match_ids)
    spike_logger.info("Finished")
    sys.exit()


async def check_bounty_in_team(match, team):
    utils = DiscordUtilities(client)
    common_db = ResourcesRequest()
    bnty_db = Bounty()
    rsrc = discord_resources.Resources()
    bnty_emoji = rsrc.get_bounty_emoji()
    platform = match.get_platform()
    platform_id = common_db.get_platform_id(platform)

    bnty_ids = bnty_db.get_bountied_player_in_team(team.get_id(), platform_id, match.get_finish_datetime(df="%Y-%m-%d"))

    if len(bnty_ids) > 0:
        for player in team.get_players():
            if player is not None and player.get_id() in bnty_ids:
                bnty_ids.remove(player.get_id())
                spp = player.get_spp() + player.get_spp_gain()
                bnty_db.update_bounty_data(player, match, team)

                if player.is_dead():
                    bnty_data = bnty_db.get_bounty_data(player.get_id(), platform_id)
                    bnty_db.update_bounty_status(player.get_id(), platform_id, BountyStatus.DEAD)

                    bnty_message = "{} **Dead Bounty** {}\n{} - {}\n".format(bnty_emoji, bnty_emoji, match.get_league_name(), match.get_competition_name())

                    bnty_message += "**{} - {}** {} spp\n".format(player.get_number(), player.get_name(), spp)
                    bnty_message += "{}\n{}".format(player.get_type().split("_")[-1], "".join(player.get_skills_emoji()))

                    for issuer in bnty_data.get("issuers", []):
                        issuer = discord.utils.get(client.get_all_members(), id=issuer)
                        await utils.send_custom_message(issuer, bnty_message)
                elif player.is_badly_hurt_only():
                    bnty_db.add_badly_hurt(player.get_id(), platform_id)
                elif player.is_mng():
                    bnty_db.add_mng(player.get_id(), platform_id)

        if len(bnty_ids) > 0:
            team_model = Utilities.get_team(platform_id=platform_id, team_id=team.get_id())
            if team_model is not None:
                for missing_player in bnty_ids:
                    player = team_model.get_player(missing_player)
                    if player is None:
                        bnty_data = bnty_db.get_bounty_data(missing_player, platform_id)
                        bnty_db.update_bounty_status(missing_player, platform_id, BountyStatus.FIRED)
                        player_name = bnty_data.get("name")
                        skills = ""
                        for skill in bnty_data.get("skills", []):
                            skills += common_db.get_skill_emoji(skill)
                        spp = bnty_data.get("spp", 0)

                        bnty_message = "{} :warning: **Fired Bounty** :warning:  {}\n{} - {}\n".format(bnty_emoji, bnty_emoji, match.get_league_name(), match.get_competition_name())
                        bnty_message += "**{}** {} spp\n{}".format(player_name, spp, skills)

                        for issuer in bnty_data.get("issuers", []):
                            issuer = discord.utils.get(client.get_all_members(), id=issuer)
                            await utils.send_custom_message(issuer, bnty_message)
            else:
                for missing_player in bnty_ids:
                    bnty_data = bnty_db.get_bounty_data(missing_player, platform_id)
                    for issuer in bnty_data.get("issuers", []):
                        issuer = discord.utils.get(client.get_all_members(), id=issuer)
                        player_name = bnty_data.get("name")
                        bnty_message = "Error during bounty check, you should check in Spike website\nhttps://spike.ovh/mybounty\n"
                        bnty_message += "{} Unable to check player: **{}** from **{}** {}\n{} - {}".format(bnty_emoji, player_name, team.get_name(), bnty_emoji, match.get_league_name(), match.get_competition_name())
                        await utils.send_custom_message(issuer, bnty_message)


sys.excepthook = except_hook

if __name__ == "__main__":
    main()
