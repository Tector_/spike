#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

import discord

from __init__ import spike_logger, init_log_file_handler, except_hook, intents
from spike_settings.SpikeSettings import SpikeSettings
from discord_resources.Utilities import DiscordUtilities
from spike_database.DiscordGuildInfo import DiscordGuildInfo

client = discord.Client(intents=intents)

# Avoid restart the update in case of web socket restart
Already_running = False


def main():
    init_log_file_handler("discord_guild_update")
    client.run(SpikeSettings.get_discord_token())


@client.event
async def on_ready():
    global Already_running
    if not Already_running:
        spike_logger.info("discord_guild_update connected")
        Already_running = True
        await update()
    else:
        spike_logger.warning("discord_guild_update reconnect")


async def update():
    utils = DiscordUtilities(client)
    guilds_id = []
    for guild in client.guilds:
        guilds_id.append(guild.id)
        await utils.update_guild(guild)
    guild_db = DiscordGuildInfo()
    guild_db.remove_guilds(guilds_id)
    spike_logger.info("Update finished")
    sys.exit()


sys.excepthook = except_hook

if __name__ == "__main__":
    main()
