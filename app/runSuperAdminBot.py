#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from datetime import datetime

from discord.ext import commands

import spike_exception
from __init__ import spike_logger, init_log_file_handler, except_hook, intents
from goblin_spy_requester.Utilities import Utilities as GspyUtilities
from spike_database.Coaches import Coaches
from spike_database.Leagues import Leagues
from spike_database.LightMatches import LightMatches
from spike_database.ResourcesRequest import ResourcesRequest
from spike_requester.CyanideApi import CyanideApi
from spike_settings.DiscordGuildSettings import DiscordGuildSettings
from spike_settings.SpikeSettings import SpikeSettings
from spike_translation import Translator
from discord_resources.Utilities import DiscordUtilities
from spike_utilities.LeagueUtilities import LeagueUtilities
from spike_utilities.Utilities import Utilities
from spike_user_db.Users import Users, AccountLvl
from pymongo.errors import DuplicateKeyError

# Discord client to communicate with server
client = commands.Bot(command_prefix="su!", intents=intents)
client.help_command.dm_help = True

Already_running = False


def globally_check_admin(ctx):
    user_db = Users()
    current_user = user_db.get_discord_user(ctx.author.id)
    if current_user is None:
        return False

    if ctx.command.name == "link":
        if AccountLvl.LINK_ADMIN in AccountLvl(current_user.get("level")):
            return True
        return False
    elif AccountLvl.ADMIN in AccountLvl(current_user.get("level")):
        return True
    return False


client.add_check(globally_check_admin)


@client.command()
async def update_users(ctx):
    utils = DiscordUtilities(client)
    user_db = Users()
    user_db.open_database()
    user_collection = user_db.db["users"]
    users = user_collection.find({"user_name": {"$exists": False}})

    for user in users:
        discord_user = client.get_user(user.get("discord_id"))
        if discord_user is None:
            discord_user = await client.fetch_user(user.get("discord_id"))

        if discord_user is not None:
            user_collection.delete_one({"user_name": discord_user.name})
            user_db.update_user(str(user.get("_id")), discord_user.name, avatar=discord_user.avatar)
            await utils.send_custom_message(ctx.channel, discord_user.name)
    user_db.close_database()


@client.command()
async def clean_users(ctx):
    user_db = Users()
    user_db.open_database()
    user_collection = user_db.db["users"]

    try:
        resp = user_collection.create_index([("type", 1), ("user_name", 1)], unique=True)
        user_db.close_database()
        spike_logger.info(resp)
    except DuplicateKeyError as ex:
        duplicate = ex.details["keyValue"]["user_name"]
        print(duplicate)
        users = list(user_collection.find({"user_name": duplicate}))
        delete_users = []
        for user in users:
            discord_user = client.get_user(user["discord_id"])
            if discord_user is None:
                try:
                    discord_user = await client.fetch_user(user["discord_id"])
                    if user.get("blood_bowl_coaches") is None and user.get("naf_name") is None and user.get("naf_number") is None:
                        delete_users.append(user["_id"])
                        print("delete: {}".format(user))
                except:
                    if user.get("blood_bowl_coaches") is None:
                        delete_users.append(user["_id"])
                        print("delete: {}".format(user))
                    else:
                        print("enable to delete: {}".format(duplicate))
        user_collection.delete_many({"_id": {"$in": delete_users}})
        user_db.close_database()
        await clean_users(ctx)


@client.command()
async def link(ctx, mention, coach_id: int, platform="pc"):
    utils = DiscordUtilities(client)
    channel = ctx.channel
    coach_db = Coaches()
    common_db = ResourcesRequest()
    user_db = Users()
    platform_id = common_db.get_platform_id(platform)
    if ctx.guild is not None:
        settings = DiscordGuildSettings(ctx.guild.id)
    else:
        settings = DiscordGuildSettings(0)

    try:
        discord_id = Utilities.get_discord_id(mention)
        user = client.get_user(discord_id)
        if user is not None:
            if platform_id is not None:
                platform = common_db.get_platform(platform_id)
                coach_name = coach_db.get_coach_name(coach_id, platform_id)
                if coach_name is not None:
                    user_data = user_db.get_discord_user(discord_id)
                    if user_data is None:
                        ret = user_db.add_discord_user(user.name, discord_id, "", user.discriminator, user.avatar)
                        user_id = str(ret)
                    else:
                        user_id = str(user_data["_id"])
                        user_db.update_user(user_id, user_name=user.name, avatar=user.avatar)
                    user_db.add_bb_coach_link(user_id, coach_id, platform_id)

                    await utils.send_custom_message(channel, "{} Coach **{}** linked to {}".format(platform[2], coach_name, user.mention))
                    log_channel = client.get_channel(537270710649880578)
                    await utils.send_custom_message(log_channel, "{} Coach **{}** linked to {} by {}".format(platform[2], coach_name, user.mention, ctx.message.author.name))
                else:
                    raise spike_exception.CoachNotFoundError(settings.get_language(), coach_id)
            else:
                raise spike_exception.InvalidPlatformError(settings.get_language(), platform)
        else:
            # /* You have to mention a valid discord user as first argument */
            raise spike_exception.InvalidSyntaxError(settings.get_language(), Translator.tr("#_admin_bot.invalid_discord_user", settings.get_language()))
    except ValueError:
        raise spike_exception.InvalidSyntaxError(settings.get_language(), Translator.tr("#_admin_bot.invalid_discord_user", settings.get_language()))
    await utils.delete_message(ctx.message)


@client.command()
async def cabal(ctx, season_number: int, platform_id: int = 1, nb_match: int = 1000):
    """Update History db with a specific cabalvision season from goblin spy

            Will request Champion Ladder, Open Ladder, Anarchy Ladder and champions cup season - 1
    """
    if ctx.message.author.id == 315120413883760640:
        utils = DiscordUtilities(client)

        gspy_utils = GspyUtilities()
        await utils.send_custom_message(ctx.message.channel, "Add match from goblin spy...")
        gspy_utils.save_cabalvision_history_into_db(int(season_number), int(platform_id), int(nb_match))
        await utils.send_custom_message(ctx.message.channel, "Update from goblin spy finished!")


@client.command()
async def leagues(ctx, league_list, platform="pc", limit=500):
    """Update History db from Cyanide Webservices"""
    if ctx.message.author.id == 315120413883760640:
        league_list = league_list.split(",")
        await update_leagues_from_ws(ctx, league_list, platform, limit)


@client.command()
async def big_leagues(ctx, league_list, platform="pc", limit=500):
    """Update History db from Cyanide Webservices"""
    if ctx.message.author.id == 315120413883760640:
        league_list = league_list.split(",")
        await update_big_leagues_from_ws(ctx, league_list, platform, limit)


@client.command()
async def league(ctx, league_name, platform="pc", limit=500):
    """Update History db from Cyanide Webservices"""
    if ctx.message.author.id == 315120413883760640:
        await update_leagues_from_ws(ctx, [league_name], platform, limit)


@client.command()
async def ulg(ctx, league_id, platform_id=1):
    """Register league"""
    utils = DiscordUtilities(client)
    await utils.send_custom_message(ctx.message.channel, "Started")
    await LeagueUtilities.activate_league(int(league_id), int(platform_id))
    await utils.send_custom_message(ctx.message.channel, "Finished")


@client.command()
async def leave(ctx, guild_id):
    if ctx.message.author.id == 315120413883760640:
        await client.get_guild(int(guild_id)).leave()


async def update_leagues_from_ws(ctx, league_list, platform, limit):
    utils = DiscordUtilities(client)
    league_db = Leagues()
    rsrc_db = ResourcesRequest()
    light_match_db = LightMatches()

    try:
        for league_name in league_list:
            await utils.send_custom_message(ctx.message.channel, "Get matches from {} - {}".format(league_name, platform))
            end_year = datetime.now().year + 1
            for year in range(2015, end_year):
                if year == datetime.now().year:
                    end_month = datetime.now().month + 1
                else:
                    end_month = 13
                if year == 2015:
                    start_month = 5
                else:
                    start_month = 1
                for month in range(start_month, end_month):
                    start_date = "{}-{}-{}".format(str(year), str(month), "1")
                    if month == 12:
                        end_date = "{}-{}-{}".format(str(year + 1), "1", "1")
                    else:
                        end_date = "{}-{}-{}".format(str(year), str(month + 1), "1")

                    matches = CyanideApi.get_matches([league_name], platform=platform, start=start_date, end=end_date, limit=limit)
                    if matches is not None and len(matches["matches"]) > 0:
                        matches = matches["matches"]
                        formatted_matches = []
                        for match in matches:
                            match["platform"] = platform
                            light_match_db.add_match(match["uuid"], match)
                            formatted_matches.append({"match": match, "uuid": match["uuid"]})
                        await utils.send_custom_message(ctx.message.channel, "Save {} matches...".format(len(formatted_matches)))
                        Utilities.write_json_matches_into_match_history(formatted_matches)
                    else:
                        await utils.send_custom_message(ctx.message.channel, "No match found between {} and {}".format(start_date, end_date))

            platform_id = rsrc_db.get_platform_id(platform)
            league_db.update_league_last_update(league_name, platform_id)
        await utils.send_custom_message(ctx.message.channel, "Update finished!")
    except Exception as e:
        print(e)


async def update_big_leagues_from_ws(ctx, leagues_name, platform, limit):
    utils = DiscordUtilities(client)
    league_db = Leagues()
    rsrc_db = ResourcesRequest()
    light_match_db = LightMatches()

    try:
        for league_name in leagues_name:
            await utils.send_custom_message(ctx.message.channel, "Get matches from {} - {}".format(league_name, platform))

            for year in range(2015, 2019):
                for month in range(1, 13):
                    start_date_first = "{}-{}-{}".format(str(year), str(month), "1")
                    if month == 12:
                        end_date_first = "{}-{}-{}".format(str(year), "1", "15")
                        end_date_second = "{}-{}-{}".format(str(year + 1), "1", "1")
                    else:
                        end_date_first = "{}-{}-{}".format(str(year), str(month), "15")
                        end_date_second = "{}-{}-{}".format(str(year), str(month + 1), "1")

                    matches = CyanideApi.get_matches([league_name], platform=platform, start=start_date_first, end=end_date_first, limit=limit)

                    if matches is not None and len(matches["matches"]) > 0:
                        matches = matches["matches"]
                        formatted_matches = []
                        for match in matches:
                            match["platform"] = platform
                            light_match_db.add_match(match["uuid"], match)
                            formatted_matches.append({"match": match, "uuid": match["uuid"]})
                        await utils.send_custom_message(ctx.message.channel, "Save {} matches...".format(len(formatted_matches)))
                        Utilities.write_json_matches_into_match_history(formatted_matches)
                    else:
                        await utils.send_custom_message(ctx.message.channel, "No match found between {} and {}".format(start_date_first, end_date_first))

                    matches = CyanideApi.get_matches([league_name], platform=platform, start=end_date_first, end=end_date_second, limit=limit)

                    if matches is not None and len(matches["matches"]) > 0:
                        matches = matches["matches"]
                        formatted_matches = []
                        for match in matches:
                            match["platform"] = platform
                            light_match_db.add_match(match["uuid"], match)
                            formatted_matches.append({"match": match, "uuid": match["uuid"]})
                        await utils.send_custom_message(ctx.message.channel, "Save {} matches...".format(len(formatted_matches)))
                        Utilities.write_json_matches_into_match_history(formatted_matches)
                    else:
                        await utils.send_custom_message(ctx.message.channel, "No match found between {} and {}".format(end_date_first, end_date_second))

            platform_id = rsrc_db.get_platform_id(platform)
            league_db.update_league_last_update(league_name, platform_id)
        await utils.send_custom_message(ctx.message.channel, "Update finished!")
    except Exception as e:
        print(e)


def main():
    init_log_file_handler("super_admin_bot")
    client.run(SpikeSettings.get_discord_token())


@client.event
async def on_ready():
    global Already_running
    if not Already_running:
        spike_logger.info("super_admin_bot connected")
        Already_running = True
    else:
        spike_logger.warning("super_admin_bot reconnect")


sys.excepthook = except_hook

if __name__ == "__main__":
    main()
