import sys

from spike_translation import Translator


def main():
    Translator.generate("", ["spike_translation"])
    sys.exit()


if __name__ == "__main__":
    main()