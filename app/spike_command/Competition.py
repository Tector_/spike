#!/usr/bin/env python
# -*- coding: utf-8 -*-

import discord

from __init__ import spike_logger
from discord_resources import Resources
from spike_command.BaseCommand import BaseCommand
from spike_database.Competitions import Competitions
from spike_database.DiscordGuild import DiscordGuild
from spike_database.Leagues import Leagues
from spike_database.ResourcesRequest import ResourcesRequest
from spike_exception import DefaultError, SpikeError, CompetitionRegistrationError, NoCompetitionRegisteredError
from spike_exception import InvalidEmojiError, CompetitionNotFoundError, InvalidSyntaxError, InvalidChannelError
from spike_model.Competition import Competition as Competition_model
from spike_requester.CyanideApi import CyanideApi
from spike_settings.DiscordGuildSettings import DiscordGuildSettings
from spike_standings.Utilities import Utilities as StandingsUtils
from spike_translation import Translator
from discord_resources.Utilities import DiscordUtilities
from spike_utilities.FuzzyUtils import FuzzyUtils
from spike_utilities.Utilities import Utilities
from discord_write.Rank import Rank


class Competition(BaseCommand):

    def __init__(self, client):
        super(Competition, self).__init__(client)

    async def auto_register_competition(self, ctx):
        utils = DiscordUtilities(self.client)
        league_db = Leagues()
        competition_db = Competitions()
        if ctx.guild is not None:
            settings = DiscordGuildSettings(ctx.guild.id)
        else:
            settings = DiscordGuildSettings(0)

        try:
            channel = ctx.message.channel
            rsrc = Resources()
            db = DiscordGuild(ctx.guild.id)
            common_db = ResourcesRequest()
            league_list = db.get_reported_leagues()

            if len(league_list) == 0:
                # /* No league registered in spike.*/
                raise DefaultError(Translator.tr("#_competition_command.no_league_registered_error", settings.get_language()))
            for leagueId in league_list:
                league_data = db.get_league_data(leagueId)
                if league_data is not None:
                    platform_data = common_db.get_platform(league_data[3])
                    # /* In what channel you want to report match for {competition} {platform} */
                    request = Translator.tr("#_competition_command.report_channel", settings.get_language()).format(competition=league_data[2], platform=platform_data[2])
                    # /* exit */
                    cancel_word = Translator.tr("#_common_command.cancel_word", settings.get_language())
                    # /* Reply format: *#chanel_name*\n\nEnter *{cancel_word}* to cancel */
                    description = Translator.tr("#_competition_command.reply_format_channel", settings.get_language()).format(cancel_word=cancel_word)
                    report_channel = await utils.ask_channel_id(request, description, cancel_word, ctx.message.channel, ctx.message.author)
                    if report_channel is not None:

                        data = CyanideApi.get_competitions(league_data[2], platform=platform_data[1])
                        if data is not None:
                            nb_add = 0
                            for competition in data["competitions"]:
                                if competition["status"] is not None and competition["status"] < 2:
                                    nb_add += 1
                                    competition = Competition_model(competition)
                                    emoji_url = common_db.get_team_emoji_url(competition.get_league().get_logo())
                                    emoji_id = common_db.get_team_emoji(competition.get_league().get_logo())
                                    db = DiscordGuild(ctx.guild.id)
                                    db.add_competition(competition.get_id(), competition.get_name(), report_channel, emoji_url, emoji_id, platform_data[0], league_data[1])
                                    league_db.add_or_update_league(competition.get_league().get_name(), competition.get_league().get_id(), platform_data[0], competition.get_league().get_logo())
                                    competition_db.add_or_update_raw_competition(competition, platform_data[0])
                                    # /* {platform} {logo} **{competition}** successfully registered in {channel} */
                                    output = Translator.tr("#_competition_command.registration_ok", settings.get_language()).format(platform=platform_data[2], logo=emoji_id, competition=competition.get_name(), channel=self.client.get_channel(report_channel).mention)
                                    await utils.send_custom_message(channel, output)
                            # /* {number} competitions successfully added */
                            await utils.send_custom_message(channel, Translator.tr("#_competition_command.registration_ok_number").format(number=nb_add))
                        else:
                            # /* Unable to get competitions from **{league}** */
                            await utils.send_custom_message(channel, Translator.tr("#_competition_command.registration_error", settings.get_language()).format(league=league_data[2]), rsrc.get_remove_emoji())
                    else:
                        # /* **{league}** - **{platform}** not registered */
                        output = Translator.tr("#_competition_command.league_not_registered", settings.get_language()).format(league=league_data[2], platform=platform_data[1])
                        await utils.send_custom_message(ctx.message.channel, output)
                else:
                    # /* Unable to find league information */
                    output = Translator.tr("#_competition_command.no_league_data", settings.get_language())
                    await utils.send_custom_message(ctx.message.channel, output)
        except SpikeError as e:
            raise e
        except Exception as e:
            spike_logger.error("Error in autoreg {}".format(e))
            raise CompetitionRegistrationError(settings.get_language())
        await utils.delete_message(ctx.message)

    async def add_competition(self, ctx, args):
        utils = DiscordUtilities(self.client)
        league_db = Leagues()
        competition_db = Competitions()
        channel = ctx.message.channel
        if ctx.guild is not None:
            settings = DiscordGuildSettings(ctx.guild.id)
        else:
            settings = DiscordGuildSettings(0)

        try:

            args = args.split(" ")

            platform = Utilities.get_platform_in_args(args)
            if platform is None:
                platform = settings.get_platform()
            else:
                # Remove platform from args if specified
                args.remove(platform)

            if len(args) > 0:
                competition_name = " ".join(args)

                db = DiscordGuild(ctx.guild.id)
                common_db = ResourcesRequest()
                fuzz = FuzzyUtils()

                platform_list = []

                if platform == "all":
                    platform_list = common_db.get_platforms()
                else:
                    platform_id = common_db.get_platform_id(platform)
                    platform_list.append(platform_id)

                for platform_id in platform_list:
                    league_list = db.get_reported_leagues_label(platform_id)
                    platform_data = common_db.get_platform(platform_id)
                    competition = await fuzz.get_best_match_competition(league_list, competition_name, platform_data[1])
                    if competition is not None:
                        # /* What's the logo of {competition} {platform} */
                        request = Translator.tr("#_competition_command.report_logo", settings.get_language()).format(competition=competition.get_name(), platform=platform_data[2])
                        # /* default */
                        default_word = Translator.tr("#_competition_command.default_word", settings.get_language())
                        # /* Reply format: *:logo:*\n\nEnter *{default_word}* to use ingame logo  */
                        description = Translator.tr("#_competition_command.reply_format_logo", settings.get_language()).format(default_word=default_word)
                        cancel_word = Translator.tr("#_common_command.cancel_word", settings.get_language())

                        ret = await utils.ask_string(request, description, cancel_word, channel, ctx.message.author)
                        if ret is not None:
                            if ret != cancel_word:
                                league_id = db.get_league_id(competition.get_league().get_name(), platform_id)
                                league_data = db.get_league_data(league_id)
                                league_db.add_or_update_league(competition.get_league().get_name(), competition.get_league().get_id(), platform_data[0], competition.get_league().get_logo())
                                if ret == default_word:
                                    league_emoji = common_db.get_team_emoji(competition.get_league().get_logo())
                                    competition_db.add_or_update_raw_competition(competition, platform_data[0])
                                    await self.add_single_competition(ctx, competition, league_data[1], league_emoji, platform_data[1])
                                elif "<:" in ret and ">" in ret:
                                    competition_db.add_or_update_raw_competition(competition, platform_data[0], ret)
                                    await self.add_single_competition(ctx, competition, league_data[1], ret, platform_data[1])
                                else:
                                    raise InvalidEmojiError(settings.get_language())
                            else:
                                raise NoCompetitionRegisteredError(settings.get_language())
                        else:
                            raise NoCompetitionRegisteredError(settings.get_language())
                    else:
                        raise CompetitionNotFoundError(settings.get_language(),competition_name, platform_data[1])
        except SpikeError as e:
            spike_logger.error(e)
            raise e
        except Exception as e:
            spike_logger.error(e)
            raise CompetitionRegistrationError(settings.get_language())
        await utils.delete_message(ctx.message)

    async def remove_competition(self, ctx, args):
        channel = ctx.message.channel
        utils = DiscordUtilities(self.client)
        db = DiscordGuild(ctx.guild.id)
        common_db = ResourcesRequest()
        if ctx.guild is not None:
            settings = DiscordGuildSettings(ctx.guild.id)
        else:
            settings = DiscordGuildSettings(0)

        args = args.split(" ")

        platform = Utilities.get_platform_in_args(args)
        if platform is None:
            platform = settings.get_platform()
        else:
            # Remove platform from args if specified
            args.remove(platform)

        if len(args) > 0:
            competition_name = " ".join(args)

            if platform == "all":
                platform = "pc"

            platform_id = common_db.get_platform_id(platform)
            platform = common_db.get_platform(platform_id)

            # Get all competition to fuzzy the best match
            competitions = db.get_reported_competitions_label(platform_id)
            fuzzy = FuzzyUtils()
            comp_name = fuzzy.get_best_match_into_list(competition_name, competitions)
            if comp_name is not None:
                competition_name = comp_name[0]

            try:
                db = DiscordGuild(ctx.guild.id)
                db.remove_competition(competition_name, platform_id)
                # /* {platform} **{entry}** successfully removed*/
                output = Translator.tr("#_common_command.entry_removed_ok", settings.get_language()).format(platform=platform[2], entry=competition_name)
                await utils.send_custom_message(channel=channel, message=output)
            except Exception as e:
                spike_logger.error("Error in remove_competition: {}".format(e))
                # /* Unable to remove: **{entry}**  */
                output = Translator.tr("#_common_command.unable_to_remove_entry", settings.get_language()).format(entry=competition_name)
                rsrc = Resources()
                await utils.send_custom_message(channel, output, rsrc.get_remove_emoji())
        else:
            # /* Invalid competition */
            raise InvalidSyntaxError(settings.get_language(), Translator.tr("#_competition_command.invalid_competition", settings.get_language()))
        await utils.delete_message(ctx.message)

    async def remove_all_competitions(self, ctx):
        utils = DiscordUtilities(self.client)
        channel = ctx.message.channel
        if ctx.guild is not None:
            settings = DiscordGuildSettings(ctx.guild.id)
        else:
            settings = DiscordGuildSettings(0)

        try:
            db = DiscordGuild(ctx.guild.id)
            db.remove_all_competition()
            # /* **ALL** competitions successfully removed */
            output = Translator.tr("#_competition_command.all_competitions_removed", settings.get_language())
            await utils.send_custom_message(channel=channel, message=output)
        except Exception as e:
            spike_logger.error("Error in remove_all_competitions: {}".format(e))
            # /* Unable to remove all competitions */
            output = Translator.tr("#_competition_command.unable_to_remove_all_competitions", settings.get_language())
            rsrc = Resources()
            await utils.send_custom_message(channel, output, rsrc.get_remove_emoji())

        await utils.delete_message(ctx.message)

    async def competition_list(self, ctx):
        utils = DiscordUtilities(self.client)

        channel = ctx.message.channel
        if ctx.guild is not None:
            settings = DiscordGuildSettings(ctx.guild.id)
        else:
            settings = DiscordGuildSettings(0)

        try:
            db = DiscordGuild(ctx.guild.id)
            common_db = ResourcesRequest()
            list_of_league = db.get_reported_leagues()
            for league in list_of_league:
                registered_cmp = []
                # Available ingame competitions
                league_data = db.get_league_data(league)
                if league_data is not None:
                    platform_data = common_db.get_platform(league_data[3])
                    league_emoji = common_db.get_team_emoji(league_data[4])
                    output_msg = "{} {} __**{}**__".format(platform_data[2], league_emoji, league_data[2])

                    cmps_list = db.get_reported_competitions(league_data[1], platform_data[0])
                    for competition_id in cmps_list:
                        competition_data = db.get_competition_data(competition_id)
                        registered_cmp.append(competition_data[1])
                        reported_channel = self.client.get_channel(competition_data[3])
                        if reported_channel is not None:
                            output_msg += "\n▫ {} {} - {}".format(competition_data[5], competition_data[2], reported_channel.mention)
                        else:
                            # /* Invalid discord channel */
                            output_msg += "\n▫ {} {} - **{}**".format(competition_data[5], competition_data[2], Translator.tr("#_common_command.invalid_channel", settings.get_language()))
                        if len(output_msg) > 1700:
                            await utils.send_custom_message(channel, output_msg)
                            output_msg = ""

                    ingame_competitions = CyanideApi.get_competitions(league_name=league_data[2], platform=platform_data[1])
                    for competition in ingame_competitions["competitions"]:
                        competition = Competition_model(competition)
                        if competition.get_id() not in registered_cmp and competition.get_status() is not None and competition.get_status() < 2:
                            # /* Not registered in spike */
                            output_msg += "\n:no_entry_sign: {} - **{}**".format(competition.get_name(), Translator.tr("#_competition_command.not_registered", settings.get_language()))
                            if len(output_msg) > 1700:
                                await utils.send_custom_message(channel, output_msg)
                                output_msg = ""
                    if len(output_msg) > 0:
                        await utils.send_custom_message(channel, output_msg)

        except Exception as e:
            spike_logger.error("Error in competition_list: {}".format(e))
            # /* Unable to get all competitions */
            output = Translator.tr("#_competition_command.unable_to_get_all_competitions", settings.get_language())
            rsrc = Resources()
            await utils.send_custom_message(channel, output, rsrc.get_remove_emoji())

        await utils.delete_message(ctx.message)

    async def rank(self, ctx, args):
        channel = ctx.message.channel
        utils = DiscordUtilities(self.client)
        db = DiscordGuild(ctx.guild.id)
        common_db = ResourcesRequest()
        if ctx.guild is not None:
            settings = DiscordGuildSettings(ctx.guild.id)
        else:
            settings = DiscordGuildSettings(0)

        platform = None
        competition_name = args
        args = args.split(" ")

        if len(args) >= 2:
            if common_db.get_platform_id(args[-1]) is not None:
                platform = args[-1]
                competition_name = competition_name.replace(args[-1], "")

        if platform is None:
            platform = settings.get_platform()

        if platform == "all":
            platform = "pc"

        platform_id = common_db.get_platform_id(platform)

        # Get all competition to fuzzy the best match
        competitions = db.get_reported_competitions_label(platform_id)
        fuzzy = FuzzyUtils()
        comp_name = fuzzy.get_best_match_into_list(competition_name, competitions)
        if comp_name is not None:
            competition_name = comp_name[0]
            competition_id = db.get_competition_id(competition_name, platform_id)
            if competition_id is not None:
                competition_data = db.get_competition_data(competition_id)
                competition_db = Competitions()
                competition_data = competition_db.get_competition_data(competition_data[1], competition_data[6])
                if competition_data:
                    if competition_data.get("format") != "single_elimination":
                        standing, point_system, standing_source = StandingsUtils.get_standing(competition_data.get("id"), competition_data.get("platform_id"))
                        writer = Rank(self.client)
                        await writer.write(ctx, standing, point_system, competition_data)
                    else:
                        # /* You can check **{competition}** status with the link below: */
                        message = Translator.tr("#_competition_command.competition_link", settings.get_language()).format(competition=competition_name)
                        message += "\nhttps://spike.ovh/competition?competition={}&platform={}".format(competition_data.get("id"), competition_data.get("platform_id"))
                        await utils.send_custom_message(channel, message)
            else:
                raise CompetitionNotFoundError(settings.get_language(), competition_name, platform=platform)
        await utils.delete_message(ctx.message)

    async def add_single_competition(self, ctx, competition, league_id, emoji_id, platform):
        utils = DiscordUtilities(self.client)
        if ctx.guild is not None:
            settings = DiscordGuildSettings(ctx.guild.id)
        else:
            settings = DiscordGuildSettings(0)

        request = Translator.tr("#_competition_command.report_channel", settings.get_language()).format(competition=competition.get_name(), platform=platform)
        cancel_word = Translator.tr("#_common_command.cancel_word", settings.get_language())
        description = Translator.tr("#_competition_command.reply_format_channel", settings.get_language()).format(cancel_word=cancel_word)

        channel_id = await utils.ask_channel_id(request, description, cancel_word, ctx.message.channel, ctx.message.author)
        if channel_id is not None:
            db = DiscordGuild(ctx.guild.id)
            common_db = ResourcesRequest()
            platform_id = common_db.get_platform_id(platform)
            platform_data = common_db.get_platform(platform_id)
            discord_id = emoji_id
            discord_id = Utilities.get_discord_id(discord_id.split(":")[2])
            emoji_obj = discord.utils.get(self.client.emojis, id=discord_id)
            db.add_competition(competition.get_id(), competition.get_name(), channel_id, str(emoji_obj.url), emoji_id, platform_data[0], league_id)
            output = Translator.tr("#_competition_command.registration_ok", settings.get_language()).format(platform=platform_data[2], logo=emoji_id, competition=competition.get_name(), channel=self.client.get_channel(channel_id).mention)
            await utils.send_custom_message(ctx.message.channel, output)
        else:
            raise InvalidChannelError(settings.get_language(), label=competition.get_name())
