#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __init__ import spike_logger
from discord_resources import Resources
from spike_command.BaseCommand import BaseCommand
from spike_database.DiscordGuild import DiscordGuild
from spike_database.ResourcesRequest import ResourcesRequest
from spike_exception import LeagueNotFoundError
from spike_settings.DiscordGuildSettings import DiscordGuildSettings
from spike_translation import Translator
from discord_resources.Utilities import DiscordUtilities
from spike_utilities.FuzzyUtils import FuzzyUtils
from spike_utilities.Utilities import Utilities
from discord_resources.League import get_league


class League(BaseCommand):

    def __init__(self, client):
        super(League, self).__init__(client)

    async def add_league(self, ctx, args):
        utils = DiscordUtilities(self.client)
        common_db = ResourcesRequest()
        channel = ctx.message.channel
        if ctx.guild is not None:
            settings = DiscordGuildSettings(ctx.guild.id)
        else:
            settings = DiscordGuildSettings(0)

        args = args.split(" ")

        platform = Utilities.get_platform_in_args(args)
        if platform is None:
            platform = settings.get_platform()
        else:
            # Remove platform from args if specified
            args.remove(platform)

        if len(args) > 0:
            league_name = " ".join(args)

            if platform == "all":
                platform = "pc"

            leagues = await get_league(self.client, ctx, league_name, platform)

            if len(leagues) == 0:
                # /* No league added */
                output = Translator.tr("#_league_command.no_league_added", settings.get_language())
                await utils.send_custom_message(channel, output)

            for league in leagues:
                platform_data = common_db.get_platform(league.get_platform_id())
                db = DiscordGuild(ctx.guild.id)
                db.add_reported_league(league.get_name(), league.get_platform_id(), league.get_id(), league.get_logo())
                # /* {platform} {league} successfully registered */
                output = Translator.tr("#_league_command.registration_ok").format(platform=platform_data[2], league=league.get_name())
                await utils.send_custom_message(channel, output)
        await utils.delete_message(ctx.message)

    async def leagues(self, ctx):
        utils = DiscordUtilities(self.client)
        rsrc = Resources()
        channel = ctx.message.channel

        if ctx.guild is not None:
            settings = DiscordGuildSettings(ctx.guild.id)
        else:
            settings = DiscordGuildSettings(0)

        db = DiscordGuild(ctx.guild.id)
        common_db = ResourcesRequest()
        league_list = db.get_reported_leagues()
        if len(league_list) == 0:
            # /* No league registered in spike use ``@!add_lg league name`` to register a league */
            output_msg = Translator.tr("#_league_command.no_league_registered", settings.get_language())
        else:
            # /* __**Registered leagues:**__ */
            output_msg = Translator.tr("#_league_command.registered_leagues", settings.get_language())
            for currentLeague in league_list:
                league_data = db.get_league_data(currentLeague)
                platform_emoji = common_db.get_platform(league_data[3])[2]
                league_emoji = common_db.get_team_emoji(league_data[4])
                output_msg += "\n▫ {} {} {} ".format(league_data[2], league_emoji, platform_emoji)
        await utils.send_custom_message(channel, output_msg, rsrc.get_remove_emoji())
        await utils.delete_message(ctx.message)

    async def remove_league(self, ctx, args):
        channel = ctx.message.channel
        utils = DiscordUtilities(self.client)
        db = DiscordGuild(ctx.guild.id)
        common_db = ResourcesRequest()
        if ctx.guild is not None:
            settings = DiscordGuildSettings(ctx.guild.id)
        else:
            settings = DiscordGuildSettings(0)

        args = args.split(" ")

        platform = Utilities.get_platform_in_args(args)
        if platform is None:
            platform = settings.get_platform()
        else:
            # Remove platform from args if specified
            args.remove(platform)

        if len(args) > 0:
            league_name = " ".join(args)

            if platform == "all":
                platform = "pc"

            platform_id = common_db.get_platform_id(platform)
            platform_data = common_db.get_platform(platform_id)

            # Get all competition to fuzzy the best match
            leagues = db.get_reported_leagues_label(platform_id)
            fuzzy = FuzzyUtils()
            lg_name = fuzzy.get_best_match_into_list(league_name, leagues)
            if lg_name is not None:
                league_name = lg_name[0]

                try:
                    db = DiscordGuild(ctx.guild.id)
                    removed_leagues = db.remove_reported_league(league_name, platform_id)
                    for removed_lg in removed_leagues:
                        output = "{} **{}**".format(platform_data[2], league_name)
                        removed_cmps = db.remove_competition_in_league(removed_lg[1], platform_id)
                        for removed_cmp in removed_cmps:
                            output += "\n▫"
                            output += Translator.tr("#_common_command.entry_removed_ok", settings.get_language()).format(platform=removed_cmp[5], entry=removed_cmp[2])
                        output += "\n"
                        output += Translator.tr("#_common_command.entry_removed_ok", settings.get_language()).format(platform=platform_data[2], entry=league_name)
                        await utils.send_custom_message(channel, output)
                except Exception as e:
                    spike_logger.error("Error in remove_league: {}".format(e))
                    output = Translator.tr("#_common_command.unable_to_remove_entry", settings.get_language()).format(entry=league_name)
                    rsrc = Resources()
                    await utils.send_custom_message(channel, output, rsrc.get_remove_emoji())
            else:
                raise LeagueNotFoundError(settings.get_language(), league_name, platform)
        await utils.delete_message(ctx.message)

    async def remove_all_leagues(self, ctx):
        utils = DiscordUtilities(self.client)
        channel = ctx.message.channel

        if ctx.guild is not None:
            settings = DiscordGuildSettings(ctx.guild.id)
        else:
            settings = DiscordGuildSettings(0)

        try:
            db = DiscordGuild(ctx.guild.id)
            db.remove_all_competition()
            # /* **ALL** leagues successfully removed */
            output = Translator.tr("#_league_command.all_leagues_removed_ok", settings.get_language())
            await utils.send_custom_message(channel=channel, message=output)
        except Exception as e:
            spike_logger.error("Error in remove_all_leagues: {}".format(e))
            # /* Unable to remove all leagues  */
            output = Translator.tr("#_league_command.unable_to_remove_all_leagues", settings.get_language())
            rsrc = Resources()
            await utils.send_custom_message(channel, output, rsrc.get_remove_emoji())

        await utils.delete_message(ctx.message)
