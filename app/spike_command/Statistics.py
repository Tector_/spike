#!/usr/bin/env python
# -*- coding: utf-8 -*-

from spike_database.BB2Resources import BB2Resources
from spike_database.Coaches import Coaches
from spike_database.Leagues import Leagues
from spike_database.ResourcesRequest import ResourcesRequest
from spike_exception import CoachNotFoundError, NoMatchFoundError, InvalidSyntaxError, InvalidRaceError, InvalidPlatformError, LeagueNotFoundError
from spike_requester.SpikeAPI.CclAPI import CclAPI
from spike_requester.SpikeAPI.StatisticsAPI import StatisticsAPI
from spike_settings.DiscordGuildSettings import DiscordGuildSettings
from spike_translation import Translator
from discord_resources.Utilities import DiscordUtilities
from spike_utilities.FuzzyUtils import FuzzyUtils
from spike_utilities.Utilities import Utilities
import pyshorteners


class Statistics:

    def __init__(self):
        super(Statistics, self).__init__()

    @staticmethod
    def get_verbose_and_platform(ctx, args):
        args = args.split(" ")

        if "-v" in args:
            verbose = True
            # Remove -v from args if specified
            args.remove("-v")
        else:
            verbose = False

        if ctx.guild is not None:
            settings = DiscordGuildSettings(ctx.guild.id)
        else:
            settings = DiscordGuildSettings(0)

        platform = Utilities.get_platform_in_args(args)
        if platform is None:
            platform = settings.get_platform()
        else:
            # Remove platform from args if specified
            args.remove(platform)

        if platform == "all":
            platform = "pc"
        args = " ".join(args)
        return verbose, platform, args

    async def versus(self, ctx, args):
        common_db = ResourcesRequest()
        if ctx.guild is not None:
            settings = DiscordGuildSettings(ctx.guild.id)
        else:
            settings = DiscordGuildSettings(0)

        verbose, platform, args = self.get_verbose_and_platform(ctx, args)
        args = args.split(" ")

        platform_id = common_db.get_platform_id(platform)
        platform_data = common_db.get_platform(platform_id)

        if len(args) > 0:
            args = " ".join(args)
            if "," not in args:
                args = args.strip()
                split_str = args.split(" ")
            else:
                split_str = args.split(",")

            if len(split_str) > 1:
                coach1 = self.get_coach(split_str[0].strip(), platform_id)
                if coach1 is None:
                    raise CoachNotFoundError(settings.get_language(), split_str[0].strip(), platform)

                coach2 = self.get_coach(split_str[1].strip(), platform_id)
                if coach2 is None:
                    raise CoachNotFoundError(settings.get_language(), split_str[1].strip(), platform)

                coach_dict = {coach1.get("id"): coach1.get("name"), coach2.get("id"): coach2.get("name")}

                name_size = max(len(coach1.get("name")), len(coach2.get("name"))) + 2
                game_template = "**{:<" + str(name_size) + "}**  {:<4} {} {} - {} {} {:>4}  **{:>" + str(name_size) + "}**\n"

                if coach1.get("id") is not None and coach2.get("id") is not None:
                    stats = StatisticsAPI.get_versus(coach1.get("id"), coach2.get("id"), platform_id, display_match_list=verbose)

                    if stats is not None:
                        output = "{} **{}** {}-{}-{} **{}**\n".format(platform_data[2], coach1.get("name"), stats.get("versus", {}).get("win_coach1"), stats.get("versus", {}).get("draw"), stats.get("versus", {}).get("win_coach2"), coach2.get("name"))
                        if verbose:
                            output += "__**Last games:**__\n"
                            match_list = sorted(stats.get("versus", {}).get("matches", []), key=lambda i: i["date"], reverse=True)
                            for match in match_list[:15]:
                                emo1 = common_db.get_team_emoji(match["team_home"]["logo"])
                                emo2 = common_db.get_team_emoji(match["team_away"]["logo"])
                                output += game_template.format(coach_dict[match["team_home"]["coach_id"]], match["team_home"]["tv"], emo1, match["team_home"]["score"], match["team_away"]["score"], emo2, match["team_away"]["tv"], coach_dict[match["team_away"]["coach_id"]])
                        await DiscordUtilities.send_custom_message(ctx.message.channel, output)
                    else:
                        raise NoMatchFoundError(settings.get_language(), coach1.get("name"), coach2.get("name"))
                else:
                    if coach1 is None:
                        raise CoachNotFoundError(settings.get_language(), args)
                    if coach2 is None:
                        raise CoachNotFoundError(settings.get_language(), args)
            else:
                # /* Give 2 valid coach name comma separated */
                raise InvalidSyntaxError(settings.get_language(), Translator.tr("#_statistics_command.give_2_coach_name_error", settings.get_language()))

    async def win_rate(self, ctx, args, league_name: str = None):
        if ctx.guild is not None:
            settings = DiscordGuildSettings(ctx.guild.id)
        else:
            settings = DiscordGuildSettings(0)

        verbose, platform, args = self.get_verbose_and_platform(ctx, args)
        rsrc_db = ResourcesRequest()
        platform_id = rsrc_db.get_platform_id(platform)
        platform_data = rsrc_db.get_platform(platform_id)

        if platform_id is not None:
            args = args.split(",")
            if len(args) > 1:
                bb_db = BB2Resources()
                race_label = args[1]
                args.remove(race_label)
                race_label = race_label.strip()
                race_id = rsrc_db.get_race_id_with_short_name(race_label.lower())
                if race_id is None:
                    race_id = bb_db.get_race_id_with_short_name(race_label.lower())

                    if race_id is None:
                        raise InvalidRaceError(settings.get_language(), race_label)

                searched_coach = " ".join(args)
                coach = self.get_coach(searched_coach, platform_id)
                if coach is not None:
                    if league_name == "ccl_win_rate":
                        stats = CclAPI.get_win_rate(coach.get("id"), platform_id, race_id)
                    else:
                        stats = StatisticsAPI.get_win_rate(coach.get("id"), platform_id, race_id, league_name=league_name)
                    if stats is not None and stats.get("nb_match", 0) > 0:
                        matches_word = Translator.tr("#_statistics_command.matches_word", settings.get_language())
                        race_name = bb_db.get_race_label(race_id)
                        race_emo = rsrc_db.get_team_emoji(race_name + "_01")
                        output = "{} {} **{}**: {}% ({} {}) {}-{}-{}".format(platform_data[2], race_emo, coach.get("name"), stats.get("win_rate"), stats.get("nb_match"), matches_word, stats.get("nb_win"), stats.get("nb_draw"), stats.get("nb_loss"))
                        await DiscordUtilities.send_custom_message(ctx.message.channel, output)
                    else:
                        raise NoMatchFoundError(settings.get_language(), coach.get("name"))
                else:
                    raise CoachNotFoundError(settings.get_language(), searched_coach)
            else:
                top_race_limit = 3
                if league_name is None:
                    top_league_limit = 3
                else:
                    top_league_limit = 0

                searched_coach = " ".join(args)
                coach = self.get_coach(searched_coach, platform_id)
                if coach is not None:
                    if league_name == "ccl_win_rate":
                        stats = CclAPI.get_win_rate(coach.get("id"), platform_id, top_race_limit=top_race_limit)
                    else:
                        stats = StatisticsAPI.get_win_rate(coach.get("id"), platform_id, top_race_limit=top_race_limit, top_league_limit=top_league_limit, league_name=league_name)
                    if stats is not None and stats.get("nb_match", 0) > 0:
                        matches_word = Translator.tr("#_statistics_command.matches_word", settings.get_language())
                        output = "{} **{}**: {}% ({} {}) - {}-{}-{}".format(platform_data[2], coach.get("name"), stats.get("win_rate"), stats.get("nb_match"), matches_word, stats.get("nb_win"), stats.get("nb_draw"), stats.get("nb_loss"))

                        games_label = Translator.tr("#_common_command.games_label", settings.get_language())

                        # /* Top 3 race */
                        if len(stats.get("top_races", [])) > 0:
                            top_races_title = Translator.tr("#_statistics_command.top_race_title", settings.get_language())
                            output += "\n\n**{}:**".format(top_races_title)
                            for race in stats.get("top_races", []):
                                output += "\n - **{}** {}% ({} {}) {}-{}-{}".format(race.get("name"), race.get("win_rate"), race.get("nb_match"), games_label, race.get("nb_win"), race.get("nb_draw"), race.get("nb_loss"))

                        # /* Top 3 league */
                        if len(stats.get("top_leagues", [])) > 0:
                            top_leagues_title = Translator.tr("#_statistics_command.top_league_title", settings.get_language())
                            output += "\n\n**{}:**".format(top_leagues_title)
                            for league in stats.get("top_leagues", []):
                                output += "\n - **{}** {}% ({} {}) {}-{}-{}".format(league.get("name"), league.get("win_rate"), league.get("nb_match"), games_label, league.get("nb_win"), league.get("nb_draw"), league.get("nb_loss"))

                        output += "\n\nhttps://spike.ovh/coach?coach_id={}&platform_id={}".format(coach.get("id"), platform_id)

                        await DiscordUtilities.send_custom_message(ctx.message.channel, output)
                    else:
                        raise NoMatchFoundError(settings.get_language(), coach.get("name"))
                else:
                    raise CoachNotFoundError(settings.get_language(), searched_coach)
        else:
            raise InvalidPlatformError(settings.get_language(), platform)

    async def top_race(self, ctx, args: str, limit: int = 10):
        if ctx.guild is not None:
            settings = DiscordGuildSettings(ctx.guild.id)
        else:
            settings = DiscordGuildSettings(0)

        verbose, platform, args = self.get_verbose_and_platform(ctx, args)
        rsrc_db = ResourcesRequest()
        platform_id = rsrc_db.get_platform_id(platform)
        platform_data = rsrc_db.get_platform(platform_id)
        bb_db = BB2Resources()

        if platform_id is not None:
            race_label = args.strip()
            race_id = rsrc_db.get_race_id_with_short_name(race_label.lower())
            if race_id is None:
                race_id = bb_db.get_race_id_with_short_name(race_label.lower())

                if race_id is None:
                    raise InvalidRaceError(settings.get_language(), race_label)
            race_label = bb_db.get_race_label(race_id)
            stats = CclAPI.get_top_race(race_label, limit, platform_id)
            if stats.get("races_rank"):
                name_size = 0
                record_size = 0
                for team in stats.get("races_rank", {}).get(race_label, []):
                    name_size = max(name_size, len(str(team.get("coach_name", ""))))
                    size = len(str(team.get("win"))) + len(str(team.get("draw"))) + len(str(team.get("loss")))
                    record_size = max(record_size, size)
                template_name = "{:<" + str(name_size) + "}"
                template_record = "{:>" + str(record_size + 2) + "}"
                output = "**{} Top {} {}** :: {}".format(platform_data[2], limit, race_label, stats.get("name"))
                output += "```glsl"
                for idx, team in enumerate(stats.get("races_rank", {}).get(race_label, []), 1):
                    wdl = "{}-{}-{}".format(team.get("win"), team.get("draw"), team.get("loss"))
                    if team.get("win_behind_first", 0) > 0:
                        template = "\n {:>2} " + template_name + " :: {:>5} :: " + template_record + " :: {:>2} WBF"
                        output += template.format(idx, team.get("coach_name"), team.get("sort"), wdl, team.get("win_behind_first"))
                    else:
                        template = "\n {:>2} " + template_name + " :: {:>5} :: " + template_record + ""
                        output += template.format(idx, team.get("coach_name"), team.get("sort"), wdl)
                output += "```"
                s = pyshorteners.Shortener()
                url = s.isgd.short("https://spike.ovh/competition_top_races?competition={}&platform={}&race={}&limit=50".format(stats.get("id"), platform_id, race_label))
                output += "Top 50:\n{}".format(url)

                await DiscordUtilities.send_custom_message(ctx.message.channel, output)
            else:
                # TODO create a dedicated error
                raise InvalidRaceError(settings.get_language(), race_label)

    # TODO rework this
    async def top_coach(self, ctx, args):
        coach_db = Coaches()
        league_db = Leagues()
        rsrc_db = ResourcesRequest()
        fuzzy = FuzzyUtils()
        return_message = []
        if ctx.guild is not None:
            settings = DiscordGuildSettings(ctx.guild.id)
        else:
            settings = DiscordGuildSettings(0)

        verbose, platform, args = self.get_verbose_and_platform(ctx, args)
        platform_id = rsrc_db.get_platform_id(platform)
        platform_data = rsrc_db.get_platform(platform_id)

        if platform_id is not None:
            args = args.split(",")
            if len(args) > 1:
                bb_db = BB2Resources()
                race_label = args[1]
                args.remove(race_label)
                race_label = race_label.strip()
                race_id = rsrc_db.get_race_id_with_short_name(race_label.lower())
                if race_id is None:
                    race_id = bb_db.get_race_id_with_short_name(race_label.lower())

                    if race_id is None:
                        raise InvalidRaceError(settings.get_language(), race_label)
                race_label = bb_db.get_race_label(race_id)
            else:
                race_label = None

            searched_name = " ".join(args).strip()
            league_name = None
            minimum_match = 50

            if searched_name.lower() in ["open ladder", "col", "open lad", "casual"]:
                league_name = "Open Ladder"
                minimum_match = 100
            elif searched_name.lower() in ["champion ladder", "ccl", "champ ladder", "champ lad",  "ranked"]:
                league_name = "Champion Ladder"
                minimum_match = 100
            elif searched_name.lower() in ["anarchy ladder", "cal", "anarchy lad"]:
                league_name = "Anarchy Ladder"
                minimum_match = 100
            else:
                if not league_db.league_exist(searched_name, platform_id):
                    like_league = league_db.get_league_name_like(searched_name, platform_id)
                    if len(like_league) > 0:
                        if len(like_league) == 1:
                            league_name = like_league[0]
                        else:
                            res = fuzzy.get_best_match_into_list(searched_name, like_league)
                            if res is not None:
                                league_name = res[0]
                else:
                    league_name = searched_name

            if league_name is not None:
                coaches = coach_db.get_top_coach(league_name, platform_id, minimum_match, race_label)
                if len(coaches) < 10:
                    minimum_match = 20
                    coaches = coach_db.get_top_coach(league_name, platform_id, minimum_match, race_label)
                    if len(coaches) < 10:
                        minimum_match = 10
                        coaches = coach_db.get_top_coach(league_name, platform_id, minimum_match, race_label)

                # /* at least {number} games */
                at_least_n_match_label = Translator.tr("#_statistics_command.at_least_n_match", settings.get_language()).format(number=minimum_match)
                if race_label is None:
                    output = "{} {} *({})*".format(platform_data[2], league_name, at_least_n_match_label)
                else:
                    race_emo = rsrc_db.get_team_emoji(race_label + "_01")
                    output = "{} {} {} *({})*".format(platform_data[2], race_emo, league_name, at_least_n_match_label)

                output += "```"
                template = "{:<2} {:<20} {:<12} {:<5} {:<5}\n"
                games_label = Translator.tr("#_common_command.games_label", settings.get_language())
                # /* Coach */
                coach_label = Translator.tr("#_common_command.coach_label", settings.get_language())
                # /* Win rate(%) */
                win_rate_label = Translator.tr("#_statistics_command.win_rate_header", settings.get_language())
                # /* W-D-L */
                w_d_l_label = Translator.tr("#_statistics_command.w_d_l_header", settings.get_language())
                output += template.format("#", coach_label, win_rate_label, games_label, w_d_l_label)
                rank = 1
                for coach in coaches[:20]:
                    wdl = "{}-{}-{}".format(coach[4], coach[5], coach[6])
                    output += template.format(rank,  coach[1], coach[0], coach[3], wdl)
                    rank += 1
                output += "```"
                return_message.append(output)
            else:
                raise LeagueNotFoundError(settings.get_language(), searched_name, platform)
        return return_message

    @staticmethod
    def get_coach(searched_name, platform_id):
        fuzzy = FuzzyUtils()
        coach_db = Coaches()
        coaches = coach_db.get_coaches_like(searched_name, platform_id, False)
        coaches_name = []
        for coach in coaches:
            coaches_name.append(coach.get("name"))
        ret = None
        if len(coaches_name) > 0:
            if len(coaches_name) == 1:
                ret = coaches_name[0]
            else:
                res = fuzzy.get_best_match_into_list(searched_name, coaches_name)
                if res is not None:
                    ret = res[0]
        if ret is not None:
            for coach in coaches:
                if ret == coach.get("name"):
                    return coach
        return ret
